﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Nt.Models
{
    public class Product
    {
        public int ProductId { get; set; }
        [Required(ErrorMessage = "Введите имя продукта")]
        [StringLength(15,  ErrorMessage = "Длина строки должна быть не более 15 символов")]
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime? CreateDate { get; set; }
        public int Price { get; set; }

        

        public virtual Category Category { get; set; }

        public virtual ICollection<Order> Order { get; set; }

        public Product()
        {
            Order = new List<Order>();
        }
    }
    
}