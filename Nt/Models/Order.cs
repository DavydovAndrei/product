﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nt.Models
{
    public class Order
    {


        public int OrderID { get; set; }
        public User User { get; set; }
        public virtual ICollection<Product> Products { get; set; }

        public int NumberOfProducts { get; set; }
        public DateTime OrderDate { get; set; } = DateTime.Now;
        public Order()
        {
            Products = new List<Product>();
        }
    }
}