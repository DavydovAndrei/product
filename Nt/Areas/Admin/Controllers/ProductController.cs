﻿using Newtonsoft.Json;
using Nt.Models;
using Nt.ViewModel;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;

namespace Nt.Areas.Admin.Controllers
{
    public class ProductController : ApiController
    {
        ShopContext db = new ShopContext();

        public IHttpActionResult GetProduct()
        {

            return Json(db.Products.Select(p => new { Name = p.Name, InfoProdu = p.Description, Price = p.Price, Id = p.ProductId, Cat = p.Category.NameCategory }));
        }


        public IHttpActionResult GetProducts(int id)
        {

            var categoryList = db.Category.Select(c => new { c.CategoryId, c.NameCategory }).ToList();
            var product = db.Products.SingleOrDefault(pr => pr.ProductId == id);
            var response = new { ProductId = product.ProductId, NameProd = product.Name, InfoProd = product.Description, PriceProd = product.Price, Cat = categoryList, CategoryId = product.Category.CategoryId };
            string json = JsonConvert.SerializeObject(response);
            return Json(json);
        }
        //
        [HttpPost]
        public void CreateProduct([FromBody]Product product)
        {
            db.Products.Add(product);
            db.SaveChanges();
        }


        [HttpPut]
        public void EditProduct(int id, [FromBody]Product product)
        {
            if (id == product.ProductId)
            {
                db.Entry(product).State = EntityState.Modified;

                db.SaveChanges();
            }
        }

        public void DeleteProduct(int id)
        {
            Product product = db.Products.Find(id);
            if (product != null)
            {
                db.Products.Remove(product);
                db.SaveChanges();
            }
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}