﻿using Nt.Models;
using Nt.ViewModel;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;


namespace Nt.Areas.Admin.Controllers
{
    public class AdminOrdersController : Controller
    {
        private ShopContext db = new ShopContext();

        // GET: Orders
        public ActionResult Index(
)
        {
            List<OrderIndexViewModel> listOrderViewModel = db.Order.Select(x => new OrderIndexViewModel
            {
                OrderId = x.OrderID,
                OrderDate = x.OrderDate,
                User = x.User.LastName,

                //имена продуктов передаются в лист отображения заказа короче сюда =>(OrderIndexViewModel)
                Products = x.Products.Select(s => s.Name).ToList(),
            }).ToList();
            return View(listOrderViewModel);
            
        }


        // GET: Orders/Create
        public ActionResult Create()
        {
            //из бд достаются id  и имена юзеров
            List<UserIndexViewModel> users = db.User.Select(c => new UserIndexViewModel
            {
                Id = c.Id,
                LastNameUser = c.LastName
            }).ToList();

            //из бд достаются id  и имена продуктов
            List<ProductIndexViewModel> products = db.Products.Select(c => new ProductIndexViewModel
            {
                ProductId = c.ProductId,
                NameProd = c.Name
            }).ToList();

            OrderCreateViewModel viewmodel = new OrderCreateViewModel(users, products);
            return View(viewmodel);
        }

       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(OrderCreateViewModel viewModel)
        {
            Order order = new Order();

            order.OrderDate = viewModel.OrderDate;

            foreach (var vmP in viewModel.ProductsId)
            {
                order.Products.Add(db.Products.First(x => x.ProductId == vmP));
            }
            order.User = db.User.First(x => x.Id == viewModel.Id);

            db.Order.Add(order);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = db.Order.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        // POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Order order = db.Order.Find(id);
            db.Order.Remove(order);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

    }
}