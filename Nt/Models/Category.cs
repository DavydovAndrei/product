﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nt.Models
{
    public class Category
    {
        public int CategoryId { get; set; }
        public string NameCategory { get; set;}

        public ICollection<Product> Product { get; set; }
        public Category()
        {
            Product = new List<Product>();
        }
    }
}