﻿using Nt.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Nt.ViewModel
{
    public class CategoryViewModel
    {
        
    
            public IEnumerable<Product> Product { get; set; }
            public SelectList Category { get; set; }
            
        }
    
}
