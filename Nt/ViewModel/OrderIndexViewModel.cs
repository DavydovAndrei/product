﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nt.ViewModel
{
    public class OrderIndexViewModel
    {
        public int OrderId { get; set; }
        public DateTime OrderDate { get; set; }
        public string User { get; set; }
        public List<string> Products { get; set; }
    }
}