﻿using Nt.Models;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Nt.Areas.Admin.Controllers
{
    public class CategoryController : Controller
    {
        private ShopContext db = new ShopContext();

        public ActionResult Index()
        {
            return View(db.Category.ToList());
        }

        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "NameCategory")] Category category)
        {
            if (category.NameCategory == "Андрей")
            {
                throw new ArgumentException();
            }
            if (ModelState.IsValid)
            {
                db.Category.Add(category);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(category);
        }
    }
}