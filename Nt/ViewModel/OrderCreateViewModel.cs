﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Nt.ViewModel
{
    public class OrderCreateViewModel
    {
        [Required(ErrorMessage = "Поля Дата обязательное для заполнения")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime OrderDate { get; set; } = DateTime.Now;
        public string Id { get; set; }

        public List<int> ProductsId { get; set; } //лист интовый для собирать сюда id продуктов который пользователь выбрал
        //передаем свойства юзера для выбора юзеров
        public List<UserIndexViewModel> User { get; set; }
        // передаем свойства продукта для выбора списка продуктов
        public List<ProductIndexViewModel> Product { get; set; }

        //конструктор для передачи в контроллер
        public OrderCreateViewModel(List<UserIndexViewModel> users, List<ProductIndexViewModel> products)
        {

            User = users;
            Product = products;
        }
        
        public OrderCreateViewModel()
        {

        }
    }
}