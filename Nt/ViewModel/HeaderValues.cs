﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nt.ViewModel
{
    public class HeaderValues
    {
        public string Host { get; set; }
        public string Connection { get; set; }
        public string Country { get; set; }
        public string Refer { get; set; }

    }
}