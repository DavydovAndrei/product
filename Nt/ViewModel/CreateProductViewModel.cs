﻿using Nt.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nt.ViewModel
{
    public class CreateProductViewModel
    {
        public string Name { get; set; }
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        public int Price { get; set; }
        public int SelectedCategoryId { get; set; }
        public DateTime CreateDate { get; set; } = DateTime.Now;
        public List<Category> Categories { get; set; }
        public Category Category
        {
            get; set;
        }
    }
}