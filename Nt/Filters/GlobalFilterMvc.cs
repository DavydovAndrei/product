﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Nt.Filters
{
    public class GlobalFilterMvc : FilterAttribute, IExceptionFilter
        {

            public void OnException(ExceptionContext exceptionContext)
            {
                if (!exceptionContext.ExceptionHandled && exceptionContext.Exception is ArgumentException)
                {
                exceptionContext.Result = new RedirectResult("/Content/ExceptionFound.html");
                exceptionContext.ExceptionHandled = true;

                    Logger.InitLogger();//инициализация - требуется один раз в начале
                    Logger.Log.Error("Все пропало!");
            }
            }
        }
    }
