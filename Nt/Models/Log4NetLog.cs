﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nt.Models
{
    public class Log4NetLog
    {
        public int Id {get;set;}
        public string Message { get; set; }
        public string Level { get; set; }
        public DateTime Date { get; set; }
    }
}