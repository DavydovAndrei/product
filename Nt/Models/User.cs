﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;

namespace Nt.Models
{
    public class User : IdentityUser
    {
        
        public string Name { get; set; }
        public string LastName { get; set; }
        public DateTime? DataRegistration { get; set; } = DateTime.Now;
        public ApplicationRole Role { get; set; }
        public int RoleId { get; set; }

        public virtual ICollection<Order> Order { get; set; }
    }
}