﻿using System.Globalization;
using System.Net.Http.Headers;
using System.Reflection;
using System.Web.Http.Controllers;
using System.Web.Mvc;



namespace Nt
{
    public class HeadersValueProvider : IValueProvider
    {
        readonly HttpRequestHeaders httpheaders;

        public HeadersValueProvider(HttpRequestHeaders headers)
        {
            httpheaders = headers;
        }
        PropertyInfo GetProper(string name)
        {
            var p = typeof(HttpRequestHeaders).GetProperty(name, BindingFlags.Instance | BindingFlags.Public | BindingFlags.IgnoreCase);
            return p;
        }

        public bool ContainsPrefix(string prefix)
        {
            var p = GetProper(prefix);
            return p != null;
        }

        public ValueProviderResult GetValue(string key)
        {
            var p = GetProper(key);
            if (p != null)
            {
                object value = p.GetValue(httpheaders, null);
                string s = value.ToString(); // for simplicity, convert to a string
                return new ValueProviderResult(s, s, CultureInfo.InvariantCulture);
            }
            return null;
        }

        //Это фабрика, но она не работает
        //public class HeaderValueProviderFactory : ValueProviderFactory
        //{
        //    public override IValueProvider GetValueProvider(HttpActionContext actionContext)
        //    {
        //        HttpRequestHeaders headers = actionContext.ControllerContext.Request.Headers;
        //        return new HeaderValueProvider(headers);
        //    }
        //}
    }
}
