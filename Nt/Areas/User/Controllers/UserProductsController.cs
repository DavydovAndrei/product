﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Nt.Models;
using Nt.ViewModel;


namespace Nt.Areas.User.Controllers
{
    public class UserProductsController : Controller
    {
        private ShopContext db = new ShopContext();

        // GET: Products
        public ActionResult Index(int? Id)
        {
            IQueryable<Product> Product = db.Products.Include(p => p.Category);
            if (Id != null && Id != 0)
            {
                Product = Product.Where(p => p.Category.CategoryId == Id);
            }


            List<Category> Category = db.Category.ToList();

            Category.Insert(0, new Category { NameCategory = "Все", CategoryId = 0 });

            CategoryViewModel plvm = new CategoryViewModel
            {
                Product = Product.ToList(),
                Category = new SelectList(Category, "CategoryId", "NameCategory"),

            };
            return View(plvm);
        }


        
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }
       // Function for work with basket
        public RedirectToRouteResult AddToCard(int? ProductId, string returnUrl)
        {

            Product product = db.Products.FirstOrDefault(p => p.ProductId == ProductId);
            if (product != null)
            {
                GetCard().AddItem(product, 1);
            }
            return RedirectToAction("Card", new { returnUrl });
        }

        public RedirectToRouteResult RemoveFromCard(int ProductId, string returnUrl)
        {
            Product product = db.Products.FirstOrDefault(p => p.ProductId == ProductId);
            if (product != null)
            {
                GetCard().RemoveLine(product);
            }
            return RedirectToAction("Card", new { returnUrl });
        }

        public ViewResult Card(string returnUrl)
        {
            return View(new CardIndexViewModel
            { Card = GetCard(),
            ReturnUrl = returnUrl });
        }


        public ActionResult Checkout()
        {
            Order order = new Order();
            Card card = (Card)Session["Card"];

            foreach (var vmP in card.Lines)
            {
                order.Products.Add(db.Products.First(x => x.ProductId == vmP.Product.ProductId));
            }
            order.User = db.User.Find(User.Identity.GetUserId());
            db.Order.Add(order);
            db.SaveChanges();
            Session.Clear();
            return View("Succes");

        }

        private Card GetCard()
        {
            Card card = (Card)Session["Card"];
            if (card == null)
            {
                card = new Card();
                Session["Card"] = card;
            }
            return card;
        }
    }


}
