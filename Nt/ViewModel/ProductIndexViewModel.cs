﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nt.ViewModel
{
    public class ProductIndexViewModel
    {
        public int ProductId { get; set; }
        public string NameProd { get; set; }
    }
}