﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nt.Models
{
    public class Card
    {
        private List<CardLine> lineCollection = new List<CardLine>();

        public void AddItem(Product product, int quantity)
        {
            CardLine line = lineCollection
              .Where(p => p.Product.ProductId == product.ProductId)
              .FirstOrDefault();

            if (line == null)
            {
                lineCollection.Add(new CardLine
                {
                    Product = product,
                    Quantity = quantity
                });
            }
            else
            {
                line.Quantity += quantity;
            }
        }

        public void RemoveLine(Product product)
        {
            lineCollection.RemoveAll(l => l.Product.ProductId == product.ProductId);
        }

        public decimal ComputeTotalValue()
        {
            return lineCollection.Sum(e => e.Product.Price * e.Quantity);
        }

        public void Clear()
        {
            lineCollection.Clear();
        }

        public IEnumerable<CardLine> Lines
        {
            get { return lineCollection; }

        }
    }
}