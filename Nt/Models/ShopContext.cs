﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;

namespace Nt.Models
{


    
    public class ShopContext : IdentityDbContext
    {
        public DbSet<Product> Products { get; set; }
        public DbSet<User>User { get; set; }
        public DbSet<Order> Order { get; set; }
        public DbSet<ApplicationRole> Roles { get; set; }
        public DbSet<Category> Category { get; set; }
        public DbSet<Log4NetLog> Log4NetLog { get; set; }

        public ShopContext() : base("ShopContext") { }

        public static ShopContext Create()
        {
            return new ShopContext();
        }
    }
}